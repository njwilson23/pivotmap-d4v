# Pivot Legal Mapping Project

## Setting up the environment

At time of writing, this project is performed with a combination of OGR and
Python 3 libraries. Python libraries are recorded in `requirements.txt`, so once
Python 3 and pip are available (preferably in a virtualenv), it should be enough
to
```
pip install -r requirements.txt
```

To get push access to the Gitlab repository, contact Nat (njwilson23 [snail]
gmail.com).

## Viewing the map

### Static maps

TODO

### Web maps

Right now, the web map is a [single
page](https://njwilson23.gitlab.io/pivotmap-d4v/) defined under `index.html`.

It's hosted on Gitlab, and the file `.gitlab-ci.yml` should take care of
building and uploading a new version from the repository master.

## Data collection

https://docs.google.com/spreadsheets/d/15f3No46PJF5yMKYg0ypp67rhtus2bN5pW2KsUurMN5I/edit#gid=0

### Religious orgs

Collected from Yelp searches and Yellowpages directories. We believe these data
to be incomplete.

### Schools and parks

From Vancouver open data catalogue

### Daycare

From DataBC
(https://catalogue.data.gov.bc.ca/dataset/4cc207cc-ff03-44f8-8c5f-415af5224646)
and the West Coast Child Care Resource Ceentre
(http://www.wstcoast.org/parents/lists.html)

### Residential dwellings (unfinished)

Based on city zoning, but still need to correctly identify which zones are
residential (some, such as Shaughnessy Heights, are currently excluded).

## Data processing

(see "Setting up the environment")

The data processing scripts

1. project all geographical data to a local coordinate frame
2. buffer all geographical features
3. union the buffered geometries
4. perform geometric simplification to optimize the web map
5. project back to geographical coordinates

```
./do-projection-and-buffering.sh
```
