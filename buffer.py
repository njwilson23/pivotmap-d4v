#! /usr/bin/env python
""" this script takes the latest versions of all datasets, projects them to a common projection, and buffers them at a sequence of scales.
"""

import os
import itertools
from shapely.geometry import Point, Polygon
from shapely.ops import cascaded_union
import pyproj
import picogeojson

DATASETS = {
    "dwellings": "zoning_districts_shp/zone_dwellings_dissolved_proj.geojson",
    "schools": "schools/school_parcels_proj.geojson",
    "parks": "park_polygons/park_polygons_proj.geojson",
    "daycare_wst": "daycare_wst/daycares_proj.geojson",
    "daycare_prov": "daycare_province_data_catalogue/childcare_locations_proj.geojson",
    "religious_yelp": "yelp_search/religious_orgs_proj.geojson",
    "religious_yellowpages": "religious_yp/geocodedreligious_proj.geojson",
}

# Buffer sizes, in meters
BUFFER_SCALES = [
        10,
        25,
        50,
        100,
]

EPSG_26910 = pyproj.Proj(init="epsg:26910")

def load_source(fnm):
    with open(fnm) as f:
        gj = picogeojson.fromfile(f)
    return gj

def pico_from_shapely(geom):
    return picogeojson.fromdict(geom.__geo_interface__)

def buffer_geometry(geom, scale, tol=2):
    if isinstance(geom, picogeojson.Point):
        return [Point(geom.coordinates).buffer(scale).simplify(tol)]
    elif isinstance(geom, picogeojson.Polygon):
        ring = geom.coordinates[0]
        return [Polygon(ring).buffer(scale).simplify(tol)]
    elif isinstance(geom, picogeojson.GeometryCollection):
        return list(itertools.chain(*[buffer_geometry(g, scale) for g in geom.geometries]))
    elif isinstance(geom, picogeojson.Feature):
        return buffer_geometry(geom.geometry, scale)
    elif isinstance(geom, picogeojson.FeatureCollection):
        return list(itertools.chain(*[buffer_geometry(g, scale) for g in geom.features]))
    else:
        raise ValueError(geom)

def do_buffering(name, path, scale):
    output_path = os.path.join("buffered", "{}_{}.geojson".format(name, scale))
    print("buffering {} -> {}".format(path, output_path))
    geom = load_source(path)
    buffered_geoms = buffer_geometry(geom, scale)
    unioned_geom = cascaded_union(buffered_geoms)
    final_geom = pico_from_shapely(unioned_geom).transform(lambda a: EPSG_26910(*a, inverse=True))
    picogeojson.dump(final_geom, output_path, precision=5)
    return

if __name__ == "__main__":

    for data_name, data_path in DATASETS.items():
        for scale in BUFFER_SCALES:
            do_buffering(data_name, data_path, scale)
