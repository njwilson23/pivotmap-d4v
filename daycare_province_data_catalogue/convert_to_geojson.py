import picogeojson as pico
import csv

def as_point(row):
    try:
        return pico.Point((float(row[8]), float(row[7])))
    except IndexError:
        return None

if __name__ == "__main__":

    with open("childcare_locations.csv", "r") as f:
        f.readline()        # header
        csvfile = csv.reader(f)
        daycares = [as_point(row) for row in csvfile]

    vancouver_daycares = [pt for pt in daycares if
                          pt is not None and
                          pt.coordinates[1] > 49.145 and
                          pt.coordinates[1] < 49.340 and
                          pt.coordinates[0] > -123.218 and
                          pt.coordinates[0] < -122.871]

    with open("childcare_locations.geojson", "w") as f:
        pico.dump(pico.FeatureCollection([pico.Feature(pt, {})
                                          for pt in vancouver_daycares]), f)
