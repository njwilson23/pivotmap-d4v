#! /bin/bash

PROJECTION='epsg:26910'

ogr2ogr -t_srs $PROJECTION -f GEOJSON \
    zoning_districts_shp/zone_dwellings_dissolved_proj.geojson \
    zoning_districts_shp/zone_dwellings.shp

ogr2ogr -t_srs $PROJECTION -f GEOJSON \
    schools/school_parcels_proj.geojson \
    schools/school_parcels.shp

ogr2ogr -t_srs $PROJECTION -f GEOJSON \
    park_polygons/park_polygons_proj.geojson \
    park_polygons/park_polygons.shp

ogr2ogr -t_srs $PROJECTION -f GEOJSON \
    daycare_wst/daycares_proj.geojson \
    daycare_wst/daycares.geojson

ogr2ogr -t_srs $PROJECTION -f GEOJSON \
    daycare_province_data_catalogue/childcare_locations_proj.geojson \
    daycare_province_data_catalogue/childcare_locations.geojson

ogr2ogr -t_srs $PROJECTION -f GEOJSON \
    yelp_search/religious_orgs_proj.geojson \
    yelp_search/religious_orgs.geojson

ogr2ogr -t_srs $PROJECTION -f GEOJSON \
    religious_yp/geocodedreligious_proj.geojson \
    religious_yp/geocodedreligious.geojson

python buffer.py

